const IOTA = require('iota.lib.js')
const curl = require('curl.lib.js')

const iota = new IOTA({
  provider: 'https://nodes.thetangle.org:443'
})
curl.overrideAttachToTangle(iota.api)
curl.init()

const minWeightMagnitude = 14
const depth = 3

function makeTx(seed, address) {
  const transfers = [{
    address: address,
    value: 0
  }]

  iota.api.sendTransfer(seed, depth, minWeightMagnitude, transfers, function(err, bundle) {
    console.log(err, bundle)
    console.log(`Published transaction with tail hash: ${bundle[0].hash}`)
    console.log(`Explorer link: https://thetangle.org/transaction/${bundle[0].hash}`)  
  })
}


const senderSeed = 'XDETDPOUHPRFA9GBTNTPSYWPZVHVSJQP9DZHF9YMOLPIDHYMHHNMDJLQZM9KGMZAZSUQQ9JWRBWYJLZPU'
const addressTo = '9BLAMOEYCXDBORPDYKYKQNFSDNUZJNHUQRBWBUFZQOEAAPUU9IHLDZHJHYNHVRHSBNG9KLFNH9B9IZPGBKFKKPXTNA'

makeTx(senderSeed, addressTo)
