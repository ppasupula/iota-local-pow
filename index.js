const IOTA = require('iota.lib.js')
const ccurl = require('ccurl.interface.js')

const path = './'
const minWeightMagnitude = 9
const depth = 3

const iota = new IOTA({
  provider: 'https://nodes.thetangle.org:443'
})
iota.api.attachToTangle = function(trunkTransaction, branchTransaction, minWeightMagnitude, trytes, cb) {
  const job = ccurl(trunkTransaction, branchTransaction, minWeightMagnitude, trytes, path)

  job.on('done', function(err, resp) {
    if ((resp[0]) != null) {
      cb(null, resp[0]);
    } else {
      cb(null, resp[1].trytes);
    }
  })
  
  job.start()
}

function makeTx(seed, address) {
  const transfers = [{
    address: address,
    value: 0
  }]

  iota.api.sendTransfer(seed, depth, minWeightMagnitude, transfers, function(err, bundle) {
    console.log(err, bundle)
    console.log(`Published transaction with tail hash: ${bundle[0].hash}`)
    console.log(`Explorer link: https://thetangle.org/transaction/${bundle[0].hash}`)  
  })
}


const senderSeed = 'XDETDPOUHPRFA9GBTNTPSYWPZVHVSJQP9DZHF9YMOLPIDHYMHHNMDJLQZM9KGMZAZSUQQ9JWRBWYJLZPU'
const addressTo = '9BLAMOEYCXDBORPDYKYKQNFSDNUZJNHUQRBWBUFZQOEAAPUU9IHLDZHJHYNHVRHSBNG9KLFNH9B9IZPGBKFKKPXTNA'

makeTx(senderSeed, addressTo)
