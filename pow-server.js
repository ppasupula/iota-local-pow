const { composeAPI } = require('@iota/core')
const { asciiToTrytes } = require('@iota/converter')
const ccurl = require('ccurl.interface.js')

const path = './'
const minWeightMagnitude = 9
const depth = 3

const iota = composeAPI({
  provider: 'https://nodes.devnet.iota.org:443',
  attachToTangle: function(trunkTransaction, branchTransaction, minWeightMagnitude, trytes, cb) {
    const job = ccurl(trunkTransaction, branchTransaction, minWeightMagnitude, trytes, path)

    job.on('progress', (err, progress) => {
        console.log(progress) // A number between 0 and 1 as a percentage of `trytes.length`
    })
    
    job.on('done', function(err, bundle) {
      console.log(err, bundle)
    })
    
    job.start()
  }
})

async function makeTx(seed, address) {
  const transfers = [{
    address: address,
    value: 0,
    tag: asciiToTrytes('local-pow-tx'),
    message: asciiToTrytes('Hello Local World!')
  }]


  // bundle prep for all transfers
  const trytes = await iota.prepareTransfers(seed, transfers)
  const bundle = await iota.sendTrytes(trytes, depth, minWeightMagnitude)
  console.log(`Published transaction with tail hash: ${bundle[0].hash}`)
  console.log(`Explorer link: https://devnet.thetangle.org/transaction/${bundle[0].hash}`)
}


const senderSeed = 'XDETDPOUHPRFA9GBTNTPSYWPZVHVSJQP9DZHF9YMOLPIDHYMHHNMDJLQZM9KGMZAZSUQQ9JWRBWYJLZPU'
const addressTo = '9BLAMOEYCXDBORPDYKYKQNFSDNUZJNHUQRBWBUFZQOEAAPUU9IHLDZHJHYNHVRHSBNG9KLFNH9B9IZPGBKFKKPXTNA'

makeTx(senderSeed, addressTo)
